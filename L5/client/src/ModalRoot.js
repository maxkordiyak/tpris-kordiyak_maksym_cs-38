import React, {Component} from 'react';
import { ModalConsumer } from './ModalContext';

class ModalRoot extends Component {
  render() {
    return(
      <ModalConsumer>
        {({ component: Component, props, hideModal }) =>
          Component ? <Component {...props} onRequestClose={hideModal} /> : null
        }
      </ModalConsumer>
    )
  }
}

export default ModalRoot;
