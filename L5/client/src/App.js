import React, { Component } from 'react';
import {Card, CardHeader, CardTitle, CardText, Button, CardActions, Grid, Cell, Textfield} from 'react-mdc-web/lib';
import JSONPretty from 'react-json-pretty';
import './App.css';

// const API_PATH = 'https://daring-fin-203617.appspot.com';
const API_PATH = 'https://lab06-204507.appspot.com';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      status: []
    };

    this.handleAddItems = this.handleAddItems.bind(this);
    this.handleGetStatus = this.handleGetStatus.bind(this);
  }

  handleAddItems() {
    fetch(`${API_PATH}/add`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleAddTreeItems() {
    fetch(`${API_PATH}/tree_add`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }


  handleRemoveNode() {
    fetch(`${API_PATH}/tree-drop`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleFindFirstNode() {
    fetch(`${API_PATH}/tree-find-first`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleFindAllNodes() {
    fetch(`${API_PATH}/tree-find-all`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleGetSize() {
    fetch(`${API_PATH}/getsize`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleAddThousand() {
    fetch(`${API_PATH}/addmultiple`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleGetHeadNode() {
    fetch(`${API_PATH}/getheadnode`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleInsertFirst() {
    fetch(`${API_PATH}/insertfirst`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRemoveLastItem() {
    fetch(`${API_PATH}/removelast`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRemoveOdd() {
    fetch(`${API_PATH}/remove-odd`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRemoveFirstItem() {
    fetch(`${API_PATH}/removefirst`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleGetStatus() {
    fetch(`${API_PATH}/status`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleReturnItems() {
    fetch(`${API_PATH}/return_top_item`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleLength() {
    fetch(`${API_PATH}/length`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleSave() {
    fetch(`${API_PATH}/save`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRestore() {
    fetch(`${API_PATH}/restore`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleGetTreeStatus() {
    fetch(`${API_PATH}/tree-status`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleAddNodes() {
    fetch(`${API_PATH}/tree-add`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleGetPath() {
    fetch(`${API_PATH}/tree-path`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleSplit() {
    fetch(`${API_PATH}/tree-split`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  render() {
    const { status } = this.state;

    return <div className="App">
      <header className="App-header">
        <h1 className="App-title">Lab 5 Double linked list / Tree</h1>
      </header>
      <Grid>
        <Cell col={6}><h1>Linked list</h1></Cell>
        <Cell col={6}>
          <h1>Tree</h1>
        </Cell>
        <Cell col={6}>
          <Card>
            <CardHeader>
              <CardTitle></CardTitle>
            </CardHeader>
            <CardText>
              <Button
                onClick={this.handleAddItems}
                compact>Add items</Button>
              <Button
                onClick={this.handleGetSize}
                compact>Get size of the list</Button>
              <Button
                onClick={this.handleInsertFirst}
                compact>Insert element as a first</Button>
              <Button
                onClick={this.handleGetHeadNode}
                compact>Get head node</Button>
              <Button
                onClick={this.handleRemoveLastItem}
                compact>Remove last item from the list</Button>
              <Button
                onClick={this.handleRemoveFirstItem}
                compact>Remove first item from the list</Button>
              <Button
                onClick={this.handleGetStatus}
                compact>Get status</Button>
              <Button
                onClick={this.handleRemoveOdd}
                compact>Remove odd elements from the list</Button>
              <Button
                onClick={this.handleAddThousand}
                compact>Add 1000 items</Button>
            </CardText>
            <CardActions>
            </CardActions>
          </Card>
          {status && status.length > 0 && status.map(el => <div>{el}</div>)}
        </Cell>
        <Cell col={6}>
          <Card>
            <CardHeader>
              <CardTitle></CardTitle>
            </CardHeader>
            <CardText>
              <Button
                onClick={this.handleAddTreeItems}
                compact>Add items to the tree</Button>
              <Button
                onClick={this.handleFindFirstNode}
                compact>Find the first node that match a predicate</Button>
              <Button
                onClick={this.handleFindAllNodes}
                compact>Find all nodes that match a predicate</Button>
              <Button
                onClick={this.handleRemoveNode}
                compact>Drop a node</Button>
              <Button
                onClick={this.handleGetPath}
                compact>Get node's path</Button>
                <Button
                onClick={this.handleGetTreeStatus}
                compact>Get status</Button>
              <Button
                onClick={this.handleSplit}
                compact>Split trees</Button>
            </CardText>
            <CardActions>
            </CardActions>
          </Card>
          {status && status.length > 0 && status.map(el => <div>{el}</div>)}
        </Cell>
      </Grid>
    </div>;
  }
}

export default App;
