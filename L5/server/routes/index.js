const express = require('express');
const router = express.Router();
const firebase = require('firebase');
const JSON = require('circular-json');
const LinkedList = require('./linkedlist');
const TreeModel = require('./treeModel');

if (!firebase.apps.length) {

  firebase.initializeApp({
    apiKey: "AIzaSyAYk2aVl4MpGeCmUnmB_fTMQef58Cnppps",
    authDomain: "lab05-62b2e.firebaseapp.com",
    databaseURL: "https://lab05-62b2e.firebaseio.com",
    projectId: "lab05-62b2e",
    storageBucket: "lab05-62b2e.appspot.com",
    messagingSenderId: "836523917974"
  });

}

const dbRef = firebase.database().ref('lab05-62b2e');

function fetchCurrentList(onCompleteListener) {
  dbRef.child("list").once('value', function(snapshot) {
    const childData = JSON.parse(snapshot.val());
    onCompleteListener(new LinkedList(childData));
  });
}

router.get('/add', function(req, res) {
  fetchCurrentList(function(list) {
    list.insert('data item 1');
    list.insert('data item 2');
    list.insert('data item 3');
    list.insert('data item 4');
    list.insert('data item 5');
    list.insert('data item 6');

    const stringifiedData = JSON.stringify(list);
    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully pushed items!"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/restore', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    res.json({
      success: true,
      message: `Retrieving list ${JSON.stringify(fetchedList)}`
    });
  });
});

router.get('/getsize', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    res.json({
      success: true,
      message: `The size of the list is ${fetchedList.size}`
    })
  });
});

router.get('/insertfirst', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    fetchedList.insertFirst('TESTING HEAD NODE!!!!!');
    const stringifiedData = JSON.stringify(fetchedList);

    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully inserted item"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/getheadnode', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    const headNode = fetchedList.getHeadNode();

    res.json({
      success: true,
      message: `The head node is ${JSON.stringify(headNode)}`
    })
  });
});

router.get('/removelast', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    fetchedList.remove();
    const stringifiedData = JSON.stringify(fetchedList);

    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully removed last item from the list"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/removefirst', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    fetchedList.removeFirst();
    const stringifiedData = JSON.stringify(fetchedList);

    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully removed first item from the list"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/status', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    const dataArray = [];
    fetchedList.forEach((item) => {
      dataArray.push(item.data);
    });
    res.json({
      success: true,
      message: dataArray
    })
  });
});

router.get('/remove-odd', function(req, res) {
  fetchCurrentList(function(fetchedList) {
    for(let i = fetchedList.size - 1; i >= 0; i--) {
      if(i % 2 !== 0) {
        fetchedList.removeAt(i);
      }
    }

    const stringifiedData = JSON.stringify(fetchedList);
    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully removed odd items from the list"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/addmultiple', function(req, res) {
  fetchCurrentList(function(list) {
    for (let i = 0; i < 1000; i ++) {
      list.insert(`data item ${i}`);
    }

    const stringifiedData = JSON.stringify(list);
    dbRef.child("list").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully pushed 1000 items!"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

function fetchCurrentTree(onCompleteListener) {
  dbRef.child("tree").once('value', function(snapshot) {
    const childData = snapshot.val();
    const tree = new TreeModel();
    onCompleteListener(tree, childData);
  });
};

router.get('/tree_add', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const root = tree.parse({
      id: 1,
      children: [
        {
          id: 11,
          children: [{id: 111}]
        },
        {
          id: 12,
          children: [{id: 121}, {id: 122}]
        },
        {
          id: 13
        }
      ]
    });
    const stringifiedData = JSON.stringify(root);
    dbRef.child("tree").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully created a tree!"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

router.get('/tree-find-first', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);
    const node12 = root.first(function (node) {
      return node.model.id === 12;
    });
    res.json({
      success: true,
      message: `${JSON.stringify(node12)}`
    })
  });
});

router.get('/tree-find-all', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);
    const nodesGt100 = root.all(function (node) {
      return node.model.id > 100;
    });
    res.json({
      success: true,
      message: `${JSON.stringify(nodesGt100)}`
    })
  });
});

router.get('/tree-drop', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);

    root.all(function(node) {
      return node.model.id === 121
    }).forEach(function (node) {
      node.drop();
    });
    const stringifiedData = JSON.stringify(root);
    dbRef.child("tree").set(stringifiedData)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully removed node"
        })
      })
      .catch(error => {
        console.warn(error);
      });
  });
});

// router.get('/tree-add', function(req, res) {
//   fetchCurrentTree(function(tree, childData) {
//     const treeModel = JSON.parse(childData);
//     const root = tree.parse(treeModel.model);
//
//     root.all(function(node) {
//       return node.model.id === 121
//     }).forEach(function (node) {
//       node.drop();
//     });
//     let node12 = tree.first(function (node) {
//       return node.model.id === 12;
//     });
//     const node123 = tree.parse({id: 123, children: [{id: 1231}]});
//     node12.addChild(node123);
//     const stringifiedData = JSON.stringify(root);
//     dbRef.child("tree").set(stringifiedData)
//       .then(() => {
//         res.json({
//           success: true,
//           message: "Successfully added node"
//         })
//       })
//       .catch(error => {
//         console.warn(error);
//       });
//   });
// });

router.get('/tree-path', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);
    root.all(function(node) {
      return node.model.id === 122
    }).forEach(function (node) {
      const path = node.getPath();
      res.json({
        success: true,
        message: `${JSON.stringify(path)}`
      })
    });
  });
});

router.get('/tree-status', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);

    res.json({
      success: true,
      message: `${JSON.stringify(root)}`
    })
  });
});

router.get('/tree-split', function(req, res) {
  fetchCurrentTree(function(tree, childData) {
    const treeModel = JSON.parse(childData);
    const root = tree.parse(treeModel.model);
    // const root = tree.parse({
    //   id: 1,
    //   children: [
    //     {
    //       id: 11,
    //       children: [{id: 111}]
    //     },
    //     {
    //       id: 12,
    //       children: [{id: 121}, {id: 122}]
    //     },
    //     {
    //       id: 13
    //     }
    //   ]
    // });
    // const subtree = root.model;
    // console.log(subtree)
    // function splitTree(id) {
    //   root.walk(function (node) {
    //     // Halt the traversal by returning false
    //     if (node.model.id === id) {
    //
    //     }
    //   });
    // }
    // splitTree(12);

    const array = []; // array that holds an array of names for each sublevel

    function traverse(data, level) {
      if (array[level] == undefined) array[level] = []; // if its the first time reaching this sub-level, create array
      array[level].push(data) // push the name in the sub-level array
      for(let index=0; index < data.children.length; index++){ // for each node in children
        traverse(data.children[index], level + 1); // travel the node, increasing the current sub-level
      }
    }

    traverse(root, 0); // start the recursive function
    res.json({
      success: true,
      message: `${JSON.stringify(array)}`
    })
  });
});

module.exports = router;

