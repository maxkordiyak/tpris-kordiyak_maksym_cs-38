const express = require('express');
const router = express.Router();
const firebase = require('firebase');

// router.get('/push', function(req, res) {
//   res.json({success: true})
  // const data = [7, 5, 10];
  // const dbRef = firebase.database().ref('lab04-1de79');
  // dbRef.push(data)
  //   .then(() => {
  //     res.json({
  //       success: true
  //     });
  //   })
  //   .catch(err => {
  //     res.status(500).json(err);
  //   });
// });
// firebase.initializeApp({
//   apiKey: "AIzaSyAF8Kwprax0U5X_XVuGuXA8vL0Rh2T-lpA",
//   authDomain: "lab04-1de79.firebaseapp.com",
//   databaseURL: "https://lab04-1de79.firebaseio.com",
//   projectId: "lab04-1de79",
//   storageBucket: "lab04-1de79.appspot.com",
//   messagingSenderId: "437020773719"
// });
//
// const ref = firebase.database().ref('/lab04');

function MaksymQueue(data, compare) {
  if (!(this instanceof MaksymQueue)) return new MaksymQueue(data, compare);

  this.data = data || [];
  this.length = this.data.length;
  this.compare = compare || defaultCompare;

  if (this.length > 0) {
    for (let i = (this.length >> 1) - 1; i >= 0; i--) this._down(i);
  }
}

function defaultCompare(a, b) {
  return a < b ? -1 : a > b ? 1 : 0;
}

MaksymQueue.prototype = {

  push: function (item) {
    this.data.push(item);
    this.length++;
    this._up(this.length - 1);
  },

  pop: function () {
    if (this.length === 0) return undefined;

    let top = this.data[0];
    this.length--;

    if (this.length > 0) {
      this.data[0] = this.data[this.length];
      this._down(0);
    }
    this.data.pop();

    return top;
  },

  peek: function () {
    return this.data[0];
  },

  _up: function (pos) {
    let data = this.data;
    let compare = this.compare;
    let item = data[pos];

    while (pos > 0) {
      let parent = (pos - 1) >> 1;
      let current = data[parent];
      if (compare(item, current) >= 0) break;
      data[pos] = current;
      pos = parent;
    }

    data[pos] = item;
  },

  _down: function (pos) {
    let data = this.data;
    let compare = this.compare;
    let halfLength = this.length >> 1;
    let item = data[pos];

    while (pos < halfLength) {
      let left = (pos << 1) + 1;
      let right = left + 1;
      let best = data[left];

      if (right < this.length && compare(data[right], best) < 0) {
        left = right;
        best = data[right];
      }
      if (compare(best, item) >= 0) break;

      data[pos] = best;
      pos = left;
    }

    data[pos] = item;
  }
};

let queue = new MaksymQueue();

// remove the top item
let top = queue.pop(); // returns 5

// return the top item (without removal)
top = queue.peek(); // returns 7

// get queue length
queue.length; // returns 2

// create a priority queue from an existing array (modifies the array)
queue = new MaksymQueue([7, 5, 10]);

// pass a custom item comparator as a second argument
queue = new MaksymQueue([{value: 5}, {value: 7}], function (a, b) {
  return a.value - b.value;
});

// turn a queue into a sorted array
let array = [];

while (queue.length) array.push(queue.pop());

module.exports = router;
