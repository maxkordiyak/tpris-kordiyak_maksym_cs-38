const db = firebase.firestore();

class Task {
    constructor (n,start) {
        this.n = n;
        this.start = start;
    }

    getStart () {
        return this.start;
    }

    getN () {
        return this.n;
    }

    iterative() {
        let s = 1.0;
        let x = this.getStart();
        for ( ; x <= this.getN(); x++) {
            s += ((2*x-1)/(2*x+1));
            
        }
        return s;
    }
    recursive (N) {
        let s=1.0;        
        if (N < 1) {
            return s;
        } else {
            s=((2 * N - 1)/(2 * N - 1)) + this.recursive(N-1);
        }
        return s;

    }
}
let endValue=500;
let startValue=1;
let obj = {};
let task = new Task(endValue,startValue);
let time = performance.now();
let iterative= task.iterative();
time = (performance.now() - time).toString();
console.log('execution time  ', time);
let time2 = performance.now();
let recursive= task.recursive(task.getN());
time2 = (performance.now() - time2).toString();
console.log('execution time  ', time2);
obj={
  "Operation": "sum" ,
  "Start value": startValue,
  "N": endValue ,
  "Iteration result": iterative ,
  "Recursive result": recursive,
  "Iterative time": time ,
  "Recursion time": time2,
};
console.log(obj);
db.collection("lab6").add({ task:obj })
.then(function(docRef) {
   console.log("Document written with ID: ", docRef.id);
})
.catch(function(error) {
   console.error("Error while adding document: ", error);
});