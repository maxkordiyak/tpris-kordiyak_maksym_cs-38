const express = require('express');
const router = express.Router();
var fs = require("fs");
var jsonfile = require('jsonfile');

// to run lab, enter node --max-old-space-size=16000 app.js

function shortestPath(edges, numVertices, startVertex) {
  var done = new Array(numVertices);
  done[startVertex] = true;
  var pathLengths = new Array(numVertices);
  var predecessors = new Array(numVertices);
  for (var i = 0; i < numVertices; i++) {
    pathLengths[i] = edges[startVertex][i];
    if (edges[startVertex][i] != Infinity) {
      predecessors[i] = startVertex;
    }
  }

  pathLengths[startVertex] = 0;
  for (var i = 0; i < numVertices - 1; i++) {
    var closest = -1;
    var closestDistance = Infinity;
    for (var j = 0; j < numVertices; j++) {
      if (!done[j] && pathLengths[j] < closestDistance) {
        closestDistance = pathLengths[j];
        closest = j;
      }
    }
    done[closest] = true;
    for (var j = 0; j < numVertices; j++) {
      if (!done[j]) {
        var possiblyCloserDistance = pathLengths[closest] + edges[closest][j];
        if (possiblyCloserDistance < pathLengths[j]) {
          pathLengths[j] = possiblyCloserDistance;
          predecessors[j] = closest;
        }
      }
    }
  }
  return { "startVertex": startVertex,
    "pathLengths": pathLengths,
    "predecessors": predecessors };
}

function constructPath(shortestPathInfo, endVertex) {
  var path = [];
  while (endVertex != shortestPathInfo.startVertex) {
    path.unshift(endVertex);
    endVertex = shortestPathInfo.predecessors[endVertex];
  }
  return path;
}

function makeUnweightedGraph(w, h) {
  var arr = [];
  for(i = 0; i < h; i++) {
    arr[i] = [];
    for(j = 0; j < w; j++) {
      arr[i][j] = Math.floor(Math.random() * (1-0+1)+0);
    }
  }
  return arr;
}

function makeWeightedGraph(w, h) {
  var arr = [];
  for(i = 0; i < h; i++) {
    arr[i] = [];
    for(j = 0; j < w; j++) {
      arr[i][j] = Math.floor(Math.random() * 500) - 0;
    }
  }

  return arr;
}

function calculateWeight(arr) {
  let accumulatedWeight = 0;
  for(let i = 1; i < arr.length; i++) {
    accumulatedWeight =+ arr[i - 1][i];
  }
  return accumulatedWeight;
}

let arr = makeWeightedGraph(750, 750);

let Weight = calculateWeight(arr);
let firstVertex = 2;
let secondVertex = 9;

// Compute the shortest paths from vertex number 1 to each other vertex
// in the graph.
let shortestPathInfo = shortestPath(arr, 750, firstVertex);

// Get the shortest path from vertex 1 to vertex 6.
let vertices = constructPath(shortestPathInfo, secondVertex);
if (Array.isArray(vertices)
  && vertices.length
  && vertices[vertices.length - 1] == secondVertex) {
  console.log(`There is a path between vertex ${firstVertex} and vertex ${secondVertex}`)
  } else {
  console.log(`There is no path between vertex ${firstVertex} and vertex ${secondVertex}`)
}

function toIncidenceMatrix(adjacencyMatrix) {
  var vertexCount = adjacencyMatrix.length;
  var edges = [];

  // extracting the edges from the adjacencyMatrix
  for(let i = 0; i < vertexCount; i++) {
    for(let j = 0; j < vertexCount; j++) {
      if(adjacencyMatrix[i][j] != 0) {
        edges.push([i, j]);
      }
    }
  }

  // composing the incidence matrix out of the extracted data
  var incidenceMatrix = [];
  var edgeCount = edges.length;

  for(let i = 0; i < vertexCount; i++) {
    incidenceMatrix.push([]);

    for(let j = 0; j < edgeCount; j++) {
      incidenceMatrix[i].push(edges[j].includes(i) ? 1 : 0);
    }
  }

  return incidenceMatrix;
}

router.get('/', function(req, res) {
  res.json({
    'Shortest path': vertices,
    Weight: Weight
  })
});

router.get('/save', function(req, res) {
  console.log(arr);
  const file = 'savedFile.json';
  const obj = {
    columnCount: arr.length,
    rowCount: arr[0].length,
    graph: arr
  };
  jsonfile.writeFile(file, obj, function (err) {
    console.error(err)
  });
  res.json({
    success: true,
    message: 'Saved file under savedFile.json'
  })
});

router.get('/restore', function(req, res) {
  const file = 'savedFile.json';
  jsonfile.readFile(file, function(err, obj) {
    res.json({
      success: JSON.stringify(obj)
    })
  })
});

router.get('/compose', function(req, res) {
  let incidenceMatrix = toIncidenceMatrix(arr);
  res.json({
    incidenceMatrix: incidenceMatrix
  })
});

module.exports = router;

