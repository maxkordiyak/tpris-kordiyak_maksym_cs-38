package main
import (
    "bufio"
    "fmt"
    "strconv"
    "os"
"time"
)
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func elapsed(what string) func() {
    start := time.Now()
       return func() {
            fmt.Printf("%s took %v\n", what, time.Since(start))
        }
}

func main() {


    file, err := os.Open("text.txt")
    if err != nil {
      fmt.Println(err)
      return
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanWords)

    var words []string
    var charCount = 0

    for scanner.Scan() {
    	words = append(words, scanner.Text())
    	charCount += len(scanner.Text())
    }

    fmt.Println("word list:")
    for _, word := range words {
    	fmt.Println(word)
    }
    fmt.Println("Word count:")
    fmt.Println(len(words))
    fmt.Println("Character count:")
    fmt.Println(charCount)

    f, err := os.Create("result.txt")
    t := strconv.Itoa(charCount)
    f.WriteString(t)

   defer elapsed("main")()
}