// setup firebase
import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyCgrliDW2S8BtnyDloVKelN5NHTgLKXADI",
  authDomain: "silgosp-database.firebaseapp.com",
  databaseURL: "https://silgosp-database.firebaseio.com",
  projectId: "silgosp-database",
  storageBucket: "silgosp-database.appspot.com",
  messagingSenderId: "755379252574"
};

firebase.initializeApp(config);
export default firebase;
