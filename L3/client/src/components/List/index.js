import React from 'react';
import { Link } from 'react-router-dom';
import format from 'date-fns/format';

import ReactTable from "react-table";
import matchSorter from 'match-sorter'
import "react-table/react-table.css";
import './index.css';

export default ({ list,
                  getPlacesList,
                  onFetchData,
                  goToPlace  }) => (
  <div className="list">
    <h2 className="mdc-typography--title page-title">Варіант 14 Список сільських господарств</h2>
    <ReactTable
      getTdProps={(state, rowInfo, column, instance) => {
        return {
          onClick: (e, handleOriginal) => {
            e.preventDefault();
            let placeId = rowInfo.original.id;
            goToPlace(placeId);
          }
        }
      }}
      columns={[
        {
          Header: "Name",
          accessor: "name"
        },
        {
          Header: "Income",
          accessor: "income"
        },
        {
          Header: "Workers",
          accessor: "numberOfWorkers",
        },
        {
          Header: "Production type",
          accessor: "productionType"
        },
        {
          Header: "Type of property",
          accessor: "typeOfProperty"
        }
      ]}
      data={list}
      onFetchData={() => getPlacesList()} // Request new data when things change
      filterable
      defaultPageSize={10}
      className="-striped -highlight"
    />
  </div>
);