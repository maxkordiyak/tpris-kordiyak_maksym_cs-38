import React from 'react';
import { NavLink } from 'react-router-dom';
import './index.css';

export default () => (
  [
    <NavLink key="recipes" to="/farms">Recipes</NavLink>,
    <NavLink key="new" to="/new/farm">Create</NavLink>
  ]
);