import React, { Component } from 'react';
import _ from 'lodash';
import './index.css';
import format from 'date-fns/format';
import { Button, Grid, Cell } from 'react-mdc-web/lib';
import uniqid from "uniqid";

class RecipeDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
          id: '',
          name: '',
          typeOfProperty: '',
          numberOfWorkers: '',
          productionType: '',
          income: '',
          key: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
    }

  componentDidMount() {
    console.log('props', this.props)
  }


  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.recipe, nextProps.recipe)) {
      const recipe = nextProps.recipe;
      // const oldFullVersion = Array.isArray(recipe.history) ?
      //   recipe.history["0"].changes.filter(changes => changes.path[0] === 'description') : [];
      // const oldFullDescription = oldFullVersion.map(el => el.after);
      // const oldDescriptionString = oldFullDescription.join();
      this.setState({
        id: recipe.id,
        name: recipe.name,
        typeOfProperty: recipe.typeOfProperty,
        numberOfWorkers: recipe.numberOfWorkers,
        productionType: recipe.productionType,
        income: recipe.income,
        key: recipe.key

      });
    } else {
      const recipe = this.props.recipe;
      // const oldFullVersion = Array.isArray(recipe.history) ?
      //   recipe.history["0"].changes.filter(changes => changes.path[0] === 'description') : [];
      // const oldFullDescription = oldFullVersion.map(el => el.after);
      // const oldDescriptionString = oldFullDescription.join();
      this.setState({
        id: recipe.id,
        name: recipe.name,
        typeOfProperty: recipe.typeOfProperty,
        numberOfWorkers: recipe.numberOfWorkers,
        productionType: recipe.productionType,
        income: recipe.income,
        key: recipe.key
      });
    }
  }

  onSubmit(e) {
        e.preventDefault();

        let name,
          typeOfProperty,
          created_at,
          numberOfWorkers,
          productionType,
          income;

        this.props.updateRecipe({
          id: this.state.id,
          name: this.state.name,
          typeOfProperty: this.state.typeOfProperty,
          numberOfWorkers: this.state.numberOfWorkers,
          productionType: this.state.productionType,
          income: this.state.income,
          key: this.state.key
        })
    };

    render() {
        return(
              <div className="details">
            <h2 className="mdc-typography--title page-title">Add new recipe</h2>
          <div className="details__actions">
            <a className="details__button--back" onClick={this.props.goBack}>Go Back</a>
            <a className="details__button--remove"
               onClick={() => this.props.removeRecipe(this.state.key)}>
              Delete recipe
            </a>
          </div>
          <div className="details-inner">
            <form onSubmit={this.onSubmit}>
        <Grid>

          <Cell col={6}>
            <h4>Name of a farm</h4>
        </Cell>
          <Cell col={6}>
            <div className="mdc-text-field mdc-text-field--fullwidth">
              <input type="text"
                     required
                     id="name"
                     className="mdc-text-field__input"
                     name="name"
                     placeholder="Name"
                     value={this.state.name}
                     onChange={({target : {value : name}}) => {
                       this.setState({ name })
                     }}
              />
              <div className="mdc-text-field__bottom-line"></div>
            </div>
            </Cell>

            <Cell col={6}>
              <h4>Type of property</h4>
            </Cell>
            <Cell col={6}>
            <div className="mdc-text-field mdc-text-field--fullwidth">
            <input type="text"
            required
            id="typeOfProperty"
            className="mdc-text-field__input"
            name="typeOfProperty"
            placeholder="typeOfProperty"
            value={this.state.typeOfProperty}
            onChange={({target : {value : typeOfProperty}}) => {
            this.setState({ typeOfProperty })
            }}
            />
            <div className="mdc-text-field__bottom-line"></div>
            </div>
          </Cell>

          <Cell col={6}>
            <h4>Number of workers</h4>
            </Cell>
            <Cell col={6}>
              <div className="mdc-text-field mdc-text-field--fullwidth">
                <input type="text"
                       required
                       id="numberOfWorkers"
                       className="mdc-text-field__input"
                       name="numberOfWorkers"
                       placeholder="number of workers"
                       value={this.state.numberOfWorkers}
                       onChange={({target : {value : numberOfWorkers}}) => {
                         this.setState({ numberOfWorkers })
                       }}
                />
                <div className="mdc-text-field__bottom-line"></div>
              </div>
            </Cell>

            <Cell col={6}>
            <h4>Production:</h4>
        </Cell>
          <Cell col={6}>
            <div className="mdc-text-field mdc-text-field--textarea mdc-text-field--textarea--upgraded">
                                        <textarea id="productionType"
                                                  required
                                                  id="productionType"
                                                  className="mdc-text-field__input"
                                                  name="productionType"
                                                  placeholder="Production"
                                                  value={this.state.productionType}
                                                  onChange={({target : {value : productionType}}) => {
                                                    this.setState({ productionType })
                                                  }}
                                                  rows="8"
                                                  cols="40"></textarea>
              <div className="mdc-text-field__bottom-line"></div>
            </div>
          </Cell>

          <Cell col={6}>
            <h4>Income</h4>
          </Cell>
          <Cell col={6}>
            <div className="mdc-text-field mdc-text-field--fullwidth">
              <input type="text"
                     required
                     id="income"
                     className="mdc-text-field__input"
                     name="income"
                     placeholder="income"
                     value={this.state.income}
                     onChange={({target : {value : income}}) => {
                       this.setState({ income })
                     }}
              />
              <div className="mdc-text-field__bottom-line"></div>
            </div>
          </Cell>


        </Grid>
          <div className="details-footer">
            <Button className="details-button" type="submit" raised>Update</Button>
          </div>
        </form>
        </div>
        </div>
        )
    }
}

export default RecipeDetails;

