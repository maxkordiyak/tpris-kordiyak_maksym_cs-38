import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import firebase from 'firebase'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';

const rrfConfig = {
  userProfile: 'users'
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const configureStore = middleware => compose(
  rootReducer,
  applyMiddleware(thunk),
  reactReduxFirebase(firebase, rrfConfig)
)(createStore);

export default configureStore;