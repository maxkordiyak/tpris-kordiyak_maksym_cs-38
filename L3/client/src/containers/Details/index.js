import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getRecipeDetails, removeRecipe, updateRecipe } from '../../actions';
import {withRouter} from "react-router-dom";
import Details from '../../components/Details';

// Component enhancer that loads todo into redux then into the todo prop

class DetailsContainer extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getRecipeDetails(this.props.match.params.id);
    console.log(this.props)
  }

  goBack(e) {
    e.preventDefault();
    this.props.history.goBack();
  }

  updateRecipe(data) {
    if (!data) { return false; }
    return this.props.updateRecipe(data).then(
      () => this.props.history.push('/farms')
    )
  }

  removeRecipe(id) {
    if (!id) { return false; }
    return this.props.removeRecipe(id).then(
      () => this.props.history.push('/farms')
    )
  }

  render() {
    return(
      <Details recipe={this.props.details}
               removeRecipe={(id) => this.removeRecipe(id)}
               goBack={(e) => this.goBack(e)}
               updateRecipe={(data) => this.updateRecipe(data)}
      />)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getRecipeDetails: id => dispatch(getRecipeDetails(id)),
    removeRecipe: id => dispatch(removeRecipe(id)),
    updateRecipe: data => dispatch(updateRecipe(data))
  };
};


const mapStateToProps = (state) => ({
  details: state.recipe.details
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DetailsContainer));