import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { compose } from 'redux'
import { withRouter } from 'react-router-dom';
import { getRecipeList, setPageIndex, fetchRecipesIfNeeded } from '../../actions';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import firebase from 'firebase';
import List from '../../components/List';
import ReactPaginate from 'react-paginate';

class ListContainer extends Component {
    componentWillMount() {
        console.log(this.props)
    }

    goToPlaceById(id) {
      if (!id) { return false; }
      this.props.history.push(`/farms/${id}`);
    }

    onClick() {
      firebase.watchEvent('value', 'todos')
      console.log('fired')
    }

    render() {

        let errorMsg = null;
        if (this.props.errorMessage) {
            errorMsg = <div className="message--error">{this.props.errorMessage}</div>;
        };

        return (
            <div>
              {errorMsg}
              <List
                list={this.props.farms}
                getPlacesList={this.props.getRecipeList}
                goToPlace={(id) => this.goToPlaceById(id)}
              />
            </div>
        );
  }
}


const mapDispatchToProps = dispatch => {
    return {
        getRecipeList: () => dispatch(getRecipeList()),
        setPageIndex: index => {dispatch(setPageIndex(index))}
    };
};


const mapStateToProps = (state) => ({
  list: state.recipe.list,
  pagination: state.recipe.pagination,
  farms: state.firebase.data.farms ? Object.values(state.firebase.data.farms) : []
});

export default compose(
  firebaseConnect([
    'farms'
  ]),
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ListContainer);