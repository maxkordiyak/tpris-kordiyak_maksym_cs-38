import { combineReducers } from 'redux';
import recipe from './recipe';
import { firebaseReducer } from 'react-redux-firebase';

const rootReducer = combineReducers({
  recipe,
  firebase: firebaseReducer
});

export default rootReducer;