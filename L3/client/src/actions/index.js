import { buildUrl } from '../utils/index.js';
import firebase from 'firebase'
import { firebaseConnect, getVal } from 'react-redux-firebase'

export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';

export const GET_LIST = 'GET_LIST';
export const GET_LIST_ERROR = 'GET_LIST';
export const GET_DETAILS = 'GET_DETAILS';
export const GET_DETAILS_ERROR = 'GET_DETAILS';
export const SET_INDEX = 'SET_INDEX';
export const REMOVE_RECIPE = 'REMOVE_RECIPE';

export const UPDATE_RECIPE_ERROR = 'UPDATE_RECIPE_ERROR';
export const REMOVE_RECIPE_ERROR = 'REMOVE_RECIPE_ERROR';
export const CREATE_RECIPE_ERROR = 'CREATE_RECIPE_ERROR';

const BASE_URL = 'http://localhost:3001/api/recipes';

export function startLoading() {
  return { type: START_LOADING };
}

export function endLoading() {
  return { type: END_LOADING };
}

export function setRecipeList(json) {
  return {
    type: GET_LIST,
    list: json.data,
    pagination: json.pagination
  };
}

export function onRecipeListError(error) {
  return {
    type: GET_LIST_ERROR,
    error: error
  };
}

export function getRecipeList() {

  return (dispatch, getState) => {
    dispatch(startLoading());
    const sort = getState().recipe.sort;
    const page = getState().recipe.index.selected + 1;
    const params = {
      page: JSON.stringify(page),
      limit: getState().recipe.limit,
      sort: JSON.stringify(sort)
    };
    const URL = BASE_URL;
    return fetch(
      buildUrl(URL, params), { method: "GET" })
      .then(response => response.json())
      .then(json => dispatch(setRecipeList(json)))
      .catch(error => {
        dispatch(onRecipeListError(error))
      })
      .then(endLoading);
  }
}

export function fetchRecipesIfNeeded() {
    return dispatch => {
        return dispatch(getRecipeList)
    }
}

export function setRecipeDetails(json) {
  return {
    type: GET_DETAILS,
    details: json
  };
}

export function onRecipeDetailsError(error) {
  return {
    type: GET_DETAILS_ERROR,
    error: error
  };
}

export function getRecipeDetails(id) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      const farmsRef = firebase.database().ref('farms');
      const query = farmsRef.orderByChild("id").equalTo(id)
      query.once("value", function (snapshot) {
        snapshot.forEach(function (child) {
          let farmObject = {
            key: child.key,
            id: child.val().id,
            name: child.val().name,
            income: child.val().income,
            numberOfWorkers: child.val().numberOfWorkers,
            productionType: child.val().productionType,
            typeOfProperty: child.val().typeOfProperty,
          };
          dispatch(setRecipeDetails(farmObject))
        });
      })
    })
      .catch(error => {
        dispatch(onCreateRecipeError(error))
      })
      .then(json => console.log(json))
    }
}

export function onCreateRecipeError(error) {
    return {
        type: CREATE_RECIPE_ERROR,
        error: error
    };
}

export function createRecipe(data) {
    const URL = BASE_URL;
    return dispatch => {
        return new Promise((resolve, reject) => {
          const farmsRef = firebase.database().ref('farms');
          farmsRef.push(data);
          resolve(data)
        })
            .catch(error => {
                dispatch(onCreateRecipeError(error))
            })
            .then(endLoading);
    };
}


export function onRemoveRecipeError(error) {
    return {
        type: REMOVE_RECIPE_ERROR,
        error: error
    };
}

export function removeRecipe(farmId) {
  const URL = BASE_URL;
  return dispatch => {
    return new Promise((resolve, reject) => {
      let farmsRef = firebase.database().ref(`/farms/${farmId}`);
      farmsRef.remove();
      resolve()
    })
      .catch(error => {
        dispatch(onCreateRecipeError(error))
      })
      .then(endLoading);
  };
}

export function onUpdateRecipeError(error) {
    return {
        type: UPDATE_RECIPE_ERROR,
        error: error
    };
}

export function updateRecipe(data) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      const farmsRef = firebase.database().ref('farms');
      const query = farmsRef.child(data.key).update(data)
    })
  }
}

export function setPageIndex(index) {
    return { type: SET_INDEX, index };
}

