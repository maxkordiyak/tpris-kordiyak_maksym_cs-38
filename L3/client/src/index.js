import React from 'react';
import { render } from 'react-dom';
import './index.css';
import Root from './Root';
import { createStore, applyMiddleware, compose } from 'redux';
import registerServiceWorker from './registerServiceWorker';
import rootReducer from './reducers';
import firebase from 'firebase'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import thunk from 'redux-thunk';

const firebaseConfig = {
  apiKey: "AIzaSyCa_xBy_vJUQUt-lGk57tYuEzL2OSVS9oc",
  authDomain: "farms-database.firebaseapp.com",
  databaseURL: "https://farms-database.firebaseio.com",
  projectId: "farms-database",
  storageBucket: "farms-database.appspot.com",
  messagingSenderId: "348644405570"
};

const rrfConfig = {
  userProfile: 'users'
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
};

const initialState = {};

const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig), // firebase instance as first argument
  // reduxFirestore(firebase) // <- needed if using firestore
  applyMiddleware(thunk),
)(createStore);

const store = createStoreWithFirebase(rootReducer, initialState)
const rootEl = document.getElementById('root');

render(<Root store={store}/>, rootEl);

registerServiceWorker();

if (module.hot) {
  module.hot.accept('./Root', () => {
    const NextRoot = require('./Root').default;

    render(<NextRoot store={store}></NextRoot>, rootEl);
  });
}