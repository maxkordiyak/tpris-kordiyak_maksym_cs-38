import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// const API_PATH = 'https://daring-fin-203617.appspot.com';
const API_PATH = 'http://localhost:4001';

class App extends Component {
  constructor(props) {
    super(props);

  }

  handlePushItems() {
    fetch(`${API_PATH}/push`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRemoveItems() {
    fetch(`${API_PATH}/remove_top_item`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleReturnItems() {
    fetch(`${API_PATH}/return_top_item`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleLength() {
    fetch(`${API_PATH}/length`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleSave() {
    fetch(`${API_PATH}/save`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  handleRestore() {
    fetch(`${API_PATH}/restore`, { method: "GET" })
      .then(response => response.json())
      .then(json => alert(json.message))
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Lab 4 Minimal priority queue</h1>
        </header>
        <p className="App-intro">
          <button onClick={this.handlePushItems}>Push items</button>
          <button onClick={this.handleRemoveItems}>Remove top item</button>
          <button onClick={this.handleReturnItems}>Return top item</button>
          <button onClick={this.handleLength}>Size of the queue</button>
          {/*<button onClick={this.handleSave}>Save to Firebase</button>*/}
          <button onClick={this.handleRestore}>Get current queue status</button>
        </p>
      </div>
    );
  }
}

export default App;
