var express = require('express');
var router = express.Router();
const firebase = require('firebase');

if (!firebase.apps.length) {

  firebase.initializeApp({
    apiKey: "AIzaSyAF8Kwprax0U5X_XVuGuXA8vL0Rh2T-lpA",
    authDomain: "lab04-1de79.firebaseapp.com",
    databaseURL: "https://lab04-1de79.firebaseio.com",
    projectId: "lab04-1de79",
    storageBucket: "lab04-1de79.appspot.com",
    messagingSenderId: "437020773719"
  });

}

var dbRef = firebase.database().ref('lab04-1de79');

function MaksymQueue(data, compare) {
  if (!(this instanceof MaksymQueue)) return new MaksymQueue(data, compare);

  this.data = data || [];
  this.length = this.data.length;
  this.compare = compare || defaultCompare;

  if (this.length > 0) {
    for (let i = (this.length >> 1) - 1; i >= 0; i--) this._down(i);
  }
}

function defaultCompare(a, b) {
  return a < b ? -1 : a > b ? 1 : 0;
}

MaksymQueue.prototype = {

  push: function (item) {
    this.data.push(item);
    this.length++;
    this._up(this.length - 1);
  },

  pop: function () {
    if (this.length === 0) return undefined;

    let top = this.data[0];
    this.length--;

    if (this.length > 0) {
      this.data[0] = this.data[this.length];
      this._down(0);
    }
    this.data.pop();

    return top;
  },

  peek: function () {
    return this.data[0];
  },

  _up: function (pos) {
    let data = this.data;
    let compare = this.compare;
    let item = data[pos];

    while (pos > 0) {
      let parent = (pos - 1) >> 1;
      let current = data[parent];
      if (compare(item, current) >= 0) break;
      data[pos] = current;
      pos = parent;
    }

    data[pos] = item;
  },

  _down: function (pos) {
    let data = this.data;
    let compare = this.compare;
    let halfLength = this.length >> 1;
    let item = data[pos];

    while (pos < halfLength) {
      let left = (pos << 1) + 1;
      let right = left + 1;
      let best = data[left];

      if (right < this.length && compare(data[right], best) < 0) {
        left = right;
        best = data[right];
      }
      if (compare(best, item) >= 0) break;

      data[pos] = best;
      pos = left;
    }

    data[pos] = item;
  }
};

router.get('/push', function(req, res, next) {
  fetchCurrentQueue(function(fetchedQueue) {
    fetchedQueue.push(3);
    fetchedQueue.push(2);
    fetchedQueue.push(1);
    fetchedQueue.push(6);
    fetchedQueue.push(10);
    fetchedQueue.push(77);
    fetchedQueue.push(3);
    fetchedQueue.push(21);
    dbRef.set(fetchedQueue.data)
      .then(() => {
        res.json({
          success: true,
          message: "Successfully pushed items!"
        })
      })
      .catch(error => {
        console.warn(error);
      })
  });
});

// router.get('/push', function(req, res, next) {
//   const data = [7, 5, 10];
//   dbRef.push({ queue: data })
//     .then(() => {
//       res.json({
//         success: true
//       });
//     })
//     .catch(err => {
//       res.status(500).json(err);
//     });
// });

router.get('/save', function(req, res) {
  fetchCurrentQueue(function(fetchedQueue) {

    dbRef.set(fetchedQueue.data)
      .then(() => {
        res.json({
          success: true,
          message: fetchedQueue.data
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });
});

router.get('/restore', function(req, res, next) {
  fetchCurrentQueue(function(fetchedQueue) {
    res.json({
      success: true,
      message: `Retrieving queue ${fetchedQueue.data}`
    });
  });
});

function fetchCurrentQueue(onCompleteListener) {
  dbRef.once('value', function(snapshot) {
    const childData = snapshot.val();
    onCompleteListener(new MaksymQueue(childData));
  });
}

router.get('/remove_top_item', function(req, res, next) {
  fetchCurrentQueue(function(fetchedQueue) {
    let top = fetchedQueue.pop();
    dbRef.set(fetchedQueue.data)
      .then(() => {
        res.json({
          success: true,
          message: `Top item is ${top} (removed)`
        });
      })
      .catch(error => {
        console.warn("error while removing item", error);
      })
  });
});


router.get('/return_top_item', function(req, res, next) {
  fetchCurrentQueue(function(fetchedQueue) {
    let top = fetchedQueue.peek();
      res.json({
        success: true,
        message: `Top item is ${top}`
      });
  });
});

router.get('/length', function(req, res, next) {
  fetchCurrentQueue(function(fetchedQueue) {
        res.json({
          success: true,
          message: `The length is ${fetchedQueue.length}`
        })
  });
});


module.exports = router;
