import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { Grid, Cell, Card, Button } from 'react-mdc-web/lib';
import {getRecipeList, setPageIndex, fetchRecipesIfNeeded, endLoading, onCreateRecipeError} from '../../actions';
import Display from '../../components/Display';
import ButtonPanel from '../../components/ButtonPanel';
import calculate from '../../utils/calculate';

import './index.css';

import ReactPaginate from 'react-paginate';

class LabContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: null,
      next: null,
      operation: null,
      computedMatrix0: [],
      computedMatrix1: [],
      matricesAddButton: false,
      computedAdditionResult: [],
      matricesSubstractButton: false,
      computedSubtractionResult: [],
      matricesMultiplyButton: false,
      computedMultiplicationResult: [],
      outputText: "is 0"
    };

    this.onMatricesSave = this.onMatricesSave.bind(this);
    this.onMarticesAdd = this.onMarticesAdd.bind(this);
    this.updateOutput = this.updateOutput.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onMatricesDivide = this.onMatricesDivide.bind(this);
  }

  onChange(e) {
    this.setState({
      output: e.target.value
    })
  }

  updateOutput(e) {
    let rpnResult = this.rpnEvaluator(this.state.output);
    console.log(rpnResult);
    this.setState({ outputText: "is " + rpnResult });
    const numberToAddToArray = Number(rpnResult);

    if (this.state.computedMatrix0.length < 9) {
      let computedArray = [...this.state.computedMatrix0, numberToAddToArray];
      this.setState({
        computedMatrix0: computedArray
      });
    } else {
      let computedArray = [...this.state.computedMatrix1, numberToAddToArray];
      this.setState({
        computedMatrix1: computedArray
      });
    }
  }

  rpnEvaluator(str) {
    let stack = [];
    let opsHash = {
      "+": (val1, val2) => {
        return val1 + val2;
      },
      "-": (val1, val2) => {
        return val1 - val2;
      },
      "/": (val1, val2) => {
        return val1 / val2;
      },
      "*": (val1, val2) => {
        return val1 * val2;
      }
    };
    let callback;
    str.split(" ").forEach((unit) => {
      if (opsHash[unit]) {
        console.log(stack);
        callback = opsHash[unit];
        let val1 = stack.pop();
        let val2 = stack.pop();
        stack.push(callback(val2, val1));
      } else {
        stack.push(Number(unit));
      }
    });
    if (stack.length > 1) {
      return " not a valid rpn."
    } else {
      return stack[0];
    }
  }

  handleClick = (buttonName) => {
    let promise = new Promise((resolve, reject) => {
      resolve(this.setState(calculate(this.state, buttonName)));
    })
    .then(() => {
      if (buttonName === '=') {
        const numberToAddToArray = Number(this.state.total);

        if (this.state.computedMatrix0.length < 9) {
          let computedArray = [...this.state.computedMatrix0, numberToAddToArray];
          this.setState({
            computedMatrix0: computedArray
          });
        } else {
          let computedArray = [...this.state.computedMatrix1, numberToAddToArray];
          this.setState({
            computedMatrix1: computedArray
          });
        }
      }
      return false;
    })
      .catch(error => alert('an error occured', error));
      console.log(this.state)
  };


  onMatricesSave() {
    let data = {
      matrix0: this.state.computedMatrix0,
      matrix1: this.state.computedMatrix1
    };
    return fetch('http://localhost:3001/api/matrices', {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(() => this.setState({
        matricesAddButton: true,
        matricesSubstractButton: true,
        matricesMultiplyButton: true,
        matricesDivideButton: true
      }))
      .catch(error => {
        alert(error)
      })
  }

  onMarticesAdd() {
    return fetch('http://localhost:3001/api/matrices/add', {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(() => console.log('success'))
      .catch(error => {
        alert(error)
      })
  }

  onMatricesSubstract() {
    return fetch('http://localhost:3001/api/matrices/substract', {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(() => console.log('success'))
      .catch(error => {
        alert(error)
      })
  }


  onMatricesMultiply() {
    return fetch('http://localhost:3001/api/matrices/multiply', {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(() => console.log('success'))
      .catch(error => {
        alert(error)
      })
  }

  onMatricesDivide() {
    return fetch('http://localhost:3001/api/matrices/divide', {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(() => console.log('success'))
      .catch(error => {
        alert(error)
      })
  }

    render() {
        const { computedMatrix0, computedMatrix1, computedAdditionResult } = this.state;

        let errorMsg = null;
        if (this.props.errorMessage) {
            errorMsg = <div className="message--error">{this.props.errorMessage}</div>;
        }

        let matricesAddButton = null;
        if (this.state.matricesAddButton) {
          matricesAddButton = <Button
            onClick={this.onMarticesAdd}
            raised
            compact>Додати матриці</Button>
        }

        let matricesSubstractButton = null;
        if (this.state.matricesSubstractButton) {
          matricesSubstractButton = <Button
            onClick={this.onMatricesSubstract}
            raised
            compact>Відняти матриці</Button>
        }

      let matricesMultiplyButton = null;
      if (this.state.matricesMultiplyButton) {
        matricesMultiplyButton = <Button
          onClick={this.onMatricesMultiply}
          raised
          compact>Помножити матриці</Button>
      }

      let matricesDivideButton = null;
      if (this.state.matricesDivideButton) {
        matricesDivideButton = <Button
          onClick={this.onMatricesDivide}
          raised
          compact>Поділити матриці</Button>
      }

      let matrix0 = Array.isArray(computedMatrix0)
        && computedMatrix0.length > 0
        && computedMatrix0.map(el =>
          <Card className="matrix-cell">
            <div>{el}</div>
          </Card>
        );

        let matrix1 = Array.isArray(computedMatrix1)
          && computedMatrix1.length > 0
          && computedMatrix1.map(el =>
              <Card className="matrix-cell">
                <div>{el}</div>
              </Card>
          );

      return (
          <Grid>
            {errorMsg}
            <Cell col={9}>
              <div className="output">{this.state.outputText}</div>
              <input onChange={this.onChange}
                     className="typer"
                     type="text"
                     placeholder="for instance 1 1 + 2 -" />
              <button onClick={this.updateOutput}>Розрахувати</button>
              {/*<Display*/}
                {/*value={this.state.next || this.state.total || '0'}*/}
              {/*/>*/}
              {/*<ButtonPanel*/}
                {/*clickHandler={this.handleClick}*/}
              {/*/>*/}
            </Cell>
            <Cell col={3} style={{width: '150px'}}>
              {matrix0}
              <hr />
              {matrix1}
              <hr />
              <br />
              <Button
                onClick={this.onMatricesSave}
                raised
                compact>Зберегти</Button>
              {matricesAddButton}
              {matricesSubstractButton}
              {matricesMultiplyButton}
              {matricesDivideButton}

            </Cell>
          </Grid>
        );
  }
}


const mapDispatchToProps = dispatch => {
    return {
        getRecipeList: () => dispatch(getRecipeList()),
        setPageIndex: index => {dispatch(setPageIndex(index))}
    };
};


const mapStateToProps = (state) => ({
  list: state.recipe.list,
  pagination: state.recipe.pagination
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LabContainer));
