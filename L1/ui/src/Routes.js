import React from 'react';
import { Route } from 'react-router-dom';
import LabContainer from './containers/Lab';
import NewRecipeContainer from './containers/New';
import RecipeDetailsContainer from './containers/Details';

export default () => (
  [
    <Route key="lab" exact path="/lab" component={LabContainer}></Route>
  ]
);