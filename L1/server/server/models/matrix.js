const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseTrack = require('mongoose-track');

mongooseTrack.options = {
    track: {
        created_at: false,
        updatedAt: false
    }
};

const MatrixSchema = new Schema({
    data: Object,
    additionResult: Object,
    substractionResult: Object,
    multiplicationResult: Object,
    divisionResult: Object
},{ timestamps: { createdAt: 'created_at' } });

MatrixSchema.plugin(mongoosePaginate);
MatrixSchema.plugin(mongooseTrack.plugin);

module.exports = mongoose.models.Matrix || mongoose.model('Matrix', MatrixSchema);
