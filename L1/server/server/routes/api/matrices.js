const express = require('express');
const router = express.Router();
const _ = require('lodash');
const Matrix = require('node-matrices');

const Matrices = require('../../models/matrix');

function chunkify(data) {
  let first, second, third;
  let m, n;

  m = Math.ceil(data.length / 3);
  n = Math.ceil(2 * data.length / 3);

  first = data.slice(0, m);
  second = data.slice(m, n);
  third = data.slice(n, data.length);

  return [first, second, third];
}

router.get('/', function(req, res) {
    const { page = 1, limit = 10, query = '', sort = '' } = req.query;

  Matrices.paginate(Object.assign({}, query ? _.omitBy(JSON.parse(query), _.isEmpty) : {}), { page, limit: parseInt(limit), sort: sort ? JSON.parse(sort) : '' })
        .then(result => {
            res.json({
                data: result.docs,
                pagination: {
                    page: result.page,
                    pageCount: result.pages
                }
            });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

router.get('/add', function(req, res) {
  Matrices.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, matrices) {

    const matrixId = matrices._id;

    let m1 = new Matrix(
      chunkify(matrices.data.matrix0)
    );

    let m2 = new Matrix(
      chunkify(matrices.data.matrix1)
    );

    const additionResult = m1.add(m2);

    Matrices.findByIdAndUpdate(matrixId, Object.assign({ additionResult: additionResult }))
      .then(() => {
        res.json({
          data: additionResult,
          success: true
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });
});

router.get('/substract', function(req, res) {
  Matrices.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, matrices) {

    const matrixId = matrices._id;

    let m1 = new Matrix(
      chunkify(matrices.data.matrix0)
    );

    let m2 = new Matrix(
      chunkify(matrices.data.matrix1)
    );

    const substractionResult = m1.subtract(m2);

    Matrices.findByIdAndUpdate(matrixId, Object.assign({ substractionResult: substractionResult }))
      .then(() => {
        res.json({
          data: substractionResult,
          success: true
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });
});

router.get('/multiply', function(req, res) {
  Matrices.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, matrices) {

    const matrixId = matrices._id;

    let m1 = new Matrix(
      chunkify(matrices.data.matrix0)
    );

    let m2 = new Matrix(
      chunkify(matrices.data.matrix1)
    );

    console.log(m1)

    const multiplicationResult = m1.multiply(m2);

    Matrices.findByIdAndUpdate(matrixId, Object.assign({ multiplicationResult: multiplicationResult }))
      .then(() => {
        res.json({
          data: multiplicationResult,
          success: true
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });
});

router.get('/divide', function(req, res) {
  Matrices.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, matrices) {

    const matrixId = matrices._id;

    let m1 = new Matrix(
      chunkify(matrices.data.matrix0)
    );

    let m2 = new Matrix(
      chunkify(matrices.data.matrix1)
    );

    console.log(m1)

    const divisionResult = m1.substract(m2);

    Matrices.findByIdAndUpdate(matrixId, Object.assign({ divisionResult: divisionResult }))
      .then(() => {
        res.json({
          data: divisionResult,
          success: true
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  });
});

router.get('/:id', function(req, res) {
    const { id } = req.params;

    Recipe.findById(id)
        .exec()
        .then(recipe => {
            res.json({
                data: recipe
            });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

router.post('/', function(req, res) {
  new Matrices(Object.assign({ data: req.body }))
        .save()
        .then(() => {
            res.json({ success: true });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

router.post('/:id', function(req, res) {

    Recipe.findByIdAndUpdate(req.params.id, Object.assign(req.body))
        .then(() => {
            res.json({ success: true });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

router.delete('/:id', function(req, res) {
    Recipe.findByIdAndRemove(req.params.id)
        .then(() => {
            res.json({ success: true });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

module.exports = router;
