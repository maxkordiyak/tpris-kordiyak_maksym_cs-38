const express = require('express');
const router = express.Router();

router.use('/matrices', require('./matrices'));

module.exports = router;
