var express = require('express');
var router = express.Router();
var fs = require("fs");
var ElapsedTime = require('elapsed-time')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/strlength', function(req, res, next) {

});

var et = ElapsedTime.new().start()
const fileOperations = new Promise((resolve, reject) => {
  fs.readFile("text.txt", function (err, data) {
    if (err) throw err;
    const internals = data.toString();
    const strLength = internals.replace(/\s/g, "").length;
    resolve(strLength);
  });
}).then(strLength => {
  fs.appendFile("text.txt", `\nFile string length : ${strLength}!`, function(err) {
    if(err) {
      return console.warn(err);
    }
    console.log("The file was saved!");
    console.log(et.getValue())
  });
});

module.exports = router;
